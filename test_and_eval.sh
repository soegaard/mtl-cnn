rm ev_test
for model in out2/*.chunk.best_model
do
    python2 mtl_tagger.py TEST --model $model --test chunk:data/wsj/chunk/sec_20
    echo -n "$model" >> ev_test
    echo -n " " >> ev_test
    cat $model.test.tagged.chunk |python2 fix.py |perl conlleval.pl |head -2 |tail -1 >> ev_test
done
