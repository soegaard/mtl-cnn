
TAGDIR=../mtl_tagger/

#rnn_binned_ffix_dur_test.tsv     rnn_binned_nP1_dur_test.tsv
#rnn_binned_ffix_dur_train.tsv    rnn_binned_nP1_dur_train.tsv
#rnn_binned_fpass_dur_test.tsv    rnn_binned_nrefixations_test.tsv
#rnn_binned_fpass_dur_train.tsv   rnn_binned_nrefixations_train.tsv
#rnn_binned_nfixations_test.tsv   rnn_binned_totfix_dur_test.tsv
#rnn_binned_nfixations_train.tsv  rnn_binned_totfix_dur_train.tsv
#rnn_binned_nM1_dur_test.tsv      rnn_binned_totRegrTo_dur_test.tsv
#rnn_binned_nM1_dur_train.tsv     rnn_binned_totRegrTo_dur_train.tsv

OPT="--iters 10 --embeds senna.txt --in_dim 50 --hid_dim 50 --birnn"
#OPTIONS="--iters 30 --embeds ew30.txt --in_dim 30 --birnn --hid_dim 30"
echo baseline, senna, zd


#for data in google eng broadcast1 ; do
for data in broadcast1 broadcast2 broadcast3 ; do
    for task in ccg ; do #chunking ; do

	EXP="baseline"

	C=config_$EXP
	echo "train com data/"$data"_com_train.conll 2" > $C
	echo "train "$task" data/eng_"$task"_train.conll 2" >> $C
	echo "dev "$task" data/eng_"$task"_dev.conll 2" >> $C
	echo "dev com data/"$data"_com_dev.conll 2" >> $C

	python2.7 $TAGDIR/mtl_tagger.py --cfg $PWD/config_$EXP --out $PWD/out/out_$EXP $OPT
	for TESTFILE in $PWD/data/$data"_com_test.conll" ; do
	    python2.7 $TAGDIR/mtl_tagger.py TEST --model $PWD/out/out_$EXP.com.best_model --test com:$TESTFILE
	    echo $TESTFILE
	    python2.7 accuracy.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
	    python2.7 fscore.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
	done
	    
#	for gaze in ffix_dur fpass_dur nfixations nM1_dur nP1_dur nrefixations totfix_dur totRegrTo_dur ; do
	for gaze in fpass_dur totRegrTo_dur ; do
	    echo gazefile $gaze
	    EXP="mtl-"$gaze
	    C=config_$EXP
	    echo multi-task 2 2, senna, zd
	    
	    echo "train gaze data/rnn_binned_"$gaze"_train.tsv 2" > $C
	    echo "train "$task" data/eng_"$task"_train.conll 2" >> $C
	    echo "dev "$task" data/eng_"$task"_dev.conll 2" >> $C
	    echo "train com data/"$data"_com_train.conll 2" >> $C
	    echo "dev gaze data/rnn_binned_"$gaze"_test.tsv 2" >> $C
	    echo "dev com data/"$data"_com_dev.conll 2" >> $C
	    
	    python2.7 $TAGDIR/mtl_tagger.py --cfg $PWD/config_$EXP --out $PWD/out/out_$EXP $OPT
	    for TESTFILE in $PWD/data/$data"_com_test.conll" ; do
		python2.7 $TAGDIR/mtl_tagger.py TEST --model $PWD/out/out_$EXP.com.best_model --test com:$TESTFILE
		echo $TESTFILE
		python2.7 accuracy.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
		python2.7 fscore.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
	    done   
	    
	    echo multi-task 0 2, senna, zd
	    EXP="cascade-"$gaze
	    C=config_$EXP
	    echo "train gaze data/rnn_binned_"$gaze"_train.tsv 0" > $C
	    echo "train "$task" data/eng_"$task"_train.conll 0" >> $C
	    echo "dev "$task" data/eng_"$task"_dev.conll 0" >> $C
	    echo "train com data/"$data"_com_train.conll 2" >> $C
	    echo "dev gaze data/rnn_binned_"$gaze"_test.tsv 0" >> $C
	    echo "dev com data/"$data"_com_dev.conll 2" >> $C
	    
	    python2.7 $TAGDIR/mtl_tagger.py --cfg $PWD/config_$EXP --out $PWD/out/out_$EXP $OPT
	    for TESTFILE in $PWD/data/$data"_com_test.conll" ; do
		python2.7 $TAGDIR/mtl_tagger.py TEST --model $PWD/out/out_$EXP.com.best_model --test com:$TESTFILE
		echo $TESTFILE
		python2.7 accuracy.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
		python2.7 fscore.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
	    done
	done
    done
done

