for cfg in cfg2/*.cfg 
do
    for dim in 100 
    do
        out=${cfg/cfg2/out2}
        out=${out/cfg2/out2-bi-emb-$dim}
        touch $out.running
        #if ! [ -e $out.done ] 
        if ! [ -e $out.9.tagged.chunk ] 
            then 
                python2 mtl_tagger.py --cfg $cfg --out $out --birnn --hid_dim $dim --embeds sskip.100.vectors > $out.res 
        fi
        rm $out.running
        touch $out.done
    done
done
