L=geo
C=config
echo "train "$L" twitter_electorial_train.conll 2" > $C
echo "dev "$L" twitter_electorial_test.conll 2" >> $C
python2.7 mtl_tagger.py --cfg config --out out/out --iters 10 --embeds ../mislove/250m_rownorm.U.final --in_dim 40 --birnn --hid_dim 40
python2.7 mtl_tagger.py TEST --model out/out.$L.best_model --test $L:twitter_electorial_test.conll
PRED=out/out.$L.best_model.test.tagged.$L
python2.7 accuracy_mislove.py $PRED

