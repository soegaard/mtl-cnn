from __future__ import division
import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

#micro f1

prec,rec,pos,tot=0,0,0,0
for l in lines:
    if len(l)>1:
        if l[2]!="O":            
            if l[1]==l[2]:
                prec+=1
            pos+=1
        if l[1]!="O":
            if l[1]==l[2]:
                rec+=1
            tot+=1
prec=prec/pos
rec=rec/tot
print "fscore:", (2*prec*rec)/(prec+rec), prec, rec
