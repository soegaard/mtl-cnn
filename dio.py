from collections import *

def read_two_cols_data(fname):
    sent = []
    for line in file(fname):
        line = line.strip().lower().split()
        if not line:
            if sent: yield sent
            sent = []
        else:
            try:
                w,p = line
            except:
                print line
                raise
            sent.append((w,p))
    if sent: yield sent

class Corpora:
    def __init__(self):
        """
        each item is: (task_id, input_sequence, output_sequence)
        """
        self.items = []
        self.task_id_to_layers = defaultdict(set)
        self.vocab = Counter()
        self.task_to_labels_vocab = defaultdict(Counter)
        self.max_layer = 0
        self.label_to_id = {}
        self.ignore_label = None

    def set_ignore_label(self, label):
        """
        The ignored label does not appear in the labels vocabulary, if it exists in the corpus.
        """
        self.ignore_label = label
        self.label_to_id = {}

    # query
    def tasks(self):
        return set(self.task_to_labels_vocab.keys())

    def lvocab_for_task(self, task):
        return list(sorted([x for x in self.task_to_labels_vocab[task].keys() if x != self.ignore_label]))

    def levels_for_task(self, task):
        return self.task_id_to_layers(task)

    def label_mapper(self, task):
        if task not in self.label_to_id:
            self.label_to_id[task] = {l:i for i,l in enumerate(self.lvocab_for_task(task))}
        return self.label_to_id[task]

    # create
    def add_corpus(self, task, data):
        for sentence in data:
            self.items.append((task, [w for w,p in sentence], [p for w,p in sentence]))
            self.vocab.update([w for w,p in sentence])
            self.task_to_labels_vocab[task].update([p for w,p in sentence])

    def register_layers(self, task, levels):
        self.task_id_to_layers[task].update(levels)
        self.max_layer = max([self.max_layer] + levels)

def read_config(fname):
    config = {'train': Corpora(), 'test': Corpora(), 'dev': Corpora()}
    for line in file(fname):
        line = line.strip()
        if (not line) or line[0] == '#': continue

        role, task, source, levels = line.split()
        levels = [int(x) for x in levels.split(",")]

        corpora = config[role]
        corpora.register_layers(task, levels)
        corpora.add_corpus(task, read_two_cols_data(source))
    return config
