for cfg in cfg/*.cfg 
do
    for dim in 20 100 
    do
        out=${cfg/cfg/out}
        out=${out/cfg/out-bi-emb-$dim}
        python2 mtl_tagger.py --cfg $cfg --out $out --birnn --hid_dim $dim --embeds sskip.100.vectors > $out.res
    done
done
