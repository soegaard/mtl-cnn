
python2.7 mtl_tagger.py --cfg config_pos_xling_nov24 --out out/out_pos_xling_nov24 --iters 2 --embeds es.gouws_300.txt --in_dim 300 --birnn --hid_dim 100
python2.7 mtl_tagger.py TEST --model out/out_pos_xling_nov24.pos.best_model --test pos:data/spanish_pos_test.conll
PRED=out/out_pos_xling_nov24.pos.best_model.test.tagged.pos
python2.7 accuracy.py $PRED
python2.7 fscore.py $PRED
