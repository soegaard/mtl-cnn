from __future__ import division
import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

hit,tot=0,0
for l in lines:
    if len(l)>1:
        if l[1]==l[2]:
            hit+=1
        tot+=1
print "accuracy:", hit/tot
