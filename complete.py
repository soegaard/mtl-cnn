from __future__ import division
import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

Errors=False
t,c=0,0
for l in lines:
    if len(l)>1:
        if l[1]!=l[2]:
            Errors=True
    else:
        if not Errors:
            c+=1
        t+=1
        Errors=False

if not Errors:
    c+=1
t+=1

print "completes:", c/t
