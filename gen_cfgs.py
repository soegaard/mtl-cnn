files = {
        "chunk": ["chunk data/eng_chunking_train.conll","chunk data/eng_chunking_dev.conll"],
        "ner": ["ner data/eng_ner_train.conll","ner data/eng_ner_dev.conll"],
        "pos": ["pos data/eng_pos_train.conll","pos data/eng_pos_test.conll"] 
        }

# inidividual tasks, different depths
for which in ["chunk", "ner", "pos"]:
    for depth in 0,1,2:
        outf = file("cfg/%s%s.cfg" % (which, depth),"w")
        print >> outf, "train %s %s" % (files[which][0], depth)
        print >> outf, "dev %s %s" % (files[which][1], depth)
        outf.close()

# task pairs, different depths
for t1 in ["chunk", "ner", "pos"]:
    for t2 in ["chunk", "ner","pos"]:
        if t1 == t2: continue
        # all tasks at last depth, different depths
        for depth in 0,1,2:
            outf = file("cfg/%s%s-%s%s.cfg" % (t1, depth, t2, depth),"w")
            print >> outf, "train %s %s" % (files[t1][0], depth)
            print >> outf, "dev %s %s" % (files[t1][1], depth)
            print >> outf, "train %s %s" % (files[t2][0], depth)
            print >> outf, "dev %s %s" % (files[t2][1], depth)
            outf.close()

        # all tasks at all depths
        outf = file("cfg/%s012-%s012.cfg" % (t1, t2),"w")
        depth = "0,1,2"
        print >> outf, "train %s %s" % (files[t1][0], depth)
        print >> outf, "train %s %s" % (files[t2][0], depth)
        print >> outf, "dev %s %s" % (files[t1][1], 2)
        print >> outf, "dev %s %s" % (files[t2][1], 2)
        outf.close()

        # cascade, depth 1
        outf = file("cfg/%s0-%s1" % (t1,t2),"w")
        print >> outf, "train %s 0" % (files[t1][0])
        print >> outf, "train %s 1" % (files[t2][0])
        print >> outf, "dev %s 0" % (files[t1][1])
        print >> outf, "dev %s 1" % (files[t2][1])

# 3 tasks, different depths
# all tasks at last depth, different depths
for depth in 0,1,2:
    outf = file("cfg/pos%s-chunk%s-ner%s.cfg" % (depth, depth, depth),"w")
    for t in "pos chunk ner".split():
        print >> outf, "train %s %s" % (files[t][0], depth)
        print >> outf, "dev %s %s" % (files[t][1], depth)
    outf.close()

# all tasks at all depths
depth = "0,1,2"
outf = file("cfg/pos%s-chunk%s-ner%s.cfg" % (depth, depth, depth),"w")
for t in "pos chunk ner".split():
    print >> outf, "train %s %s" % (files[t][0], depth)
    print >> outf, "dev %s %s" % (files[t][1], depth)
outf.close()

# all tasks, diff cascades:
orders = [ (0,1,2), (0,2,1), (1,2,0), (1,0,2), (2,0,1), (2,1,0) ]
for d1,d2,d3 in orders:
    outf = file("cfg/pos%s-chunk%s-ner%s.cfg" % (d1, d2, d3),"w")
    for t,d in zip(["pos","chunk","ner"],[d1,d2,d3]):
        print >> outf, "train %s %s" % (files[t][0], d)
        print >> outf, "dev %s %s" % (files[t][1], d)
    outf.close()
