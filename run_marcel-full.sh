OPTIONS="--iters 50 --in_dim 128 --birnn --hid_dim 128"
CNNSEED=""
echo 0

# chbt.B1.test

C=config_low0

DIR="data/anselm/char-aligned/"
for t in B1 ; do
    #t=streusle
    #t=semcor
    #t=B1
    #t=chunking
    echo $t

    for s in B1 B2 B3 Ba1 Ba2 Be1 D3 D4 H1 Hk1 KA1492 Ka1 KJ1499 Le1 M10 M1 M2 M3 M4 M5 M6 M7 M8 M9 Me1 N1500 N1509 N1514 n1 N1 N2 N3 N4 s1496_97 sa1 Sa1 Sb1 SG1 St1 St2 Stu1 T1 W1 We1 ; do 
	echo "train "$s" "$DIR$s".norm.train 2" > $C
	echo "dev "$s" "$DIR$s".norm.dev 2" >> $C
        echo "train celex data/celex_german_aligned_with_markers.txt.sample 0" >> $C
	echo "dev celex data/celex_german_aligned_with_markers.txt.sample 0" >> $C
	
	python2.7 mtl_tagger.py $CNNSEED --cfg config_low0 --out out/out_low0 $OPTIONS
	python2.7 mtl_tagger.py TEST --model out/out_low0.$s.best_model --test $s:$DIR$s".norm.test"
	PRED=out/out_low0.$s.best_model.test.tagged.$s
	echo $PRED
	python2.7 accuracy.py $PRED
	python2.7 fscore.py $PRED
	python2.7 complete.py $PRED
    done
    echo 1
    
    C=config_low1
    echo "train "$t" "$DIR$t".norm.train 2" > $C
    echo "dev "$t" "$DIR$t".norm.dev 2" >> $C
    echo "train celex data/celex_german_aligned_with_markers.txt.sample 0" >> $C
    echo "dev celex data/celex_german_aligned_with_markers.txt.sample 0" >> $C
      for s in B2 B3 Ba1 Ba2 Be1 D3 D4 H1 Hk1 KA1492 Ka1 KJ1499 Le1 M10 M1 M2 M3 M4 M5 M6 M7 M8 M9 Me1 N1500 N1509 N1514 n1 N1 N2 N3 N4 s1496_97 sa1 Sa1 Sb1 SG1 St1 St2 Stu1 T1 W1 We1 ; do 
	echo "train "$s" "$DIR$s".norm.train 2" >> $C
	echo "dev "$s" "$DIR$s".norm.dev 2" >> $C
    done
    
    python2.7 mtl_tagger.py $CNNSEED --cfg config_low1 --out out/out_low1 $OPTIONS
    for s in B1 B2 B3 Ba1 Ba2 Be1 D3 D4 H1 Hk1 KA1492 Ka1 KJ1499 Le1 M10 M1 M2 M3 M4 M5 M6 M7 M8 M9 Me1 N1500 N1509 N1514 n1 N1 N2 N3 N4 s1496_97 sa1 Sa1 Sb1 SG1 St1 St2 Stu1 T1 W1 We1 ; do 
	python2.7 mtl_tagger.py TEST --model out/out_low1.$s.best_model --test $s:$DIR$s".norm.test"
	PRED=out/out_low1.$s.best_model.test.tagged.$s
	echo $PRED
	python2.7 accuracy.py $PRED
	python2.7 fscore.py $PRED
	python2.7 complete.py $PRED
    done
done
