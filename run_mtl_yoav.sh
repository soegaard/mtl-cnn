DATE="nov25"
OPTIONS="--iters 10 --embeds ../miscembeds/senna.txt --in_dim 50 --birnn --hid_dim 50"
#OPTIONS="--iters 3 --embeds ../miscembeds/ew30.txt --in_dim 30 --birnn --hid_dim 30"
for final in chunking ; do #com ner sst frame ; do
    for aux in names ; do #pos ; do
	C0=config0_$final
	echo "train "$final" data/ontonotes_"$final"_nw_wsj.conll 2" > $C0
	echo "dev "$final" data/ontonotes_"$final"_nw_p2.5_a2e.conll 2" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out0_$final"_"$DATE $OPTIONS
	for TESTFILE in data/eng_$final"_test.conll" data/ontonotes_$final"_bc_cctv.conll" data/ontonotes_$final"_bc_cnn.conll" data/ontonotes_$final"_bc_msnbc.conll" data/ontonotes_$final"_bc_p2.5_a2e.conll" data/ontonotes_$final"_bc_p2.5_c2e.conll" data/ontonotes_$final"_bc_phoenix.conll" ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out0_$final"_"$DATE.$final.best_model --test $final:$TESTFILE
	    TEST=out/out0_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C1=config1_$final
	echo "train "$aux" data/ontonotes_"$aux"_bc_cctv.conll 2" > $C1
	echo "train "$final" data/ontonotes_"$final"_nw_wsj.conll 2" >> $C1
	echo "dev "$final" data/ontonotes_"$final"_nw_p2.5_a2e.conll 2" >> $C1
	echo "dev "$aux" data/ontonotes_"$aux"_bc_cnn.conll 2" >> $C1
	python2.7 mtl_tagger.py --cfg $C1 --out out/out1_$final"_"$DATE $OPTIONS 
	for TESTFILE in data/eng_$final"_test.conll" data/ontonotes_$final"_bc_cctv.conll" data/ontonotes_$final"_bc_cnn.conll" data/ontonotes_$final"_bc_msnbc.conll" data/ontonotes_$final"_bc_p2.5_a2e.conll" data/ontonotes_$final"_bc_p2.5_c2e.conll" data/ontonotes_$final"_bc_phoenix.conll" ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out1_$final"_"$DATE.$final.best_model --test $final:$TESTFILE
	    TEST=out/out1_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C2=config2_$final
	echo "train "$aux" data/ontonotes_"$aux"_bc_cctv.conll 0" > $C2
	echo "train "$final" data/ontonotes_"$final"_nw_wsj.conll 2" >> $C2
	echo "dev "$final" data/ontonotes_"$final"_nw_p2.5_a2e.conll 2" >> $C2
	echo "dev "$aux" data/ontonotes_"$aux"_bc_cnn.conll 0" >> $C2
	python2.7 mtl_tagger.py --cfg $C2 --out out/out2_$final"_"$DATE $OPTIONS
	for TESTFILE in data/eng_$final"_test.conll" data/ontonotes_$final"_bc_cctv.conll" data/ontonotes_$final"_bc_cnn.conll" data/ontonotes_$final"_bc_msnbc.conll" data/ontonotes_$final"_bc_p2.5_a2e.conll" data/ontonotes_$final"_bc_p2.5_c2e.conll" data/ontonotes_$final"_bc_phoenix.conll" ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out2_$final"_"$DATE.$final.best_model --test $final:$TESTFILE
	    TEST=out/out2_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	    done
    done
done
