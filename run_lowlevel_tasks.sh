OPTIONS="--iters 10 --embeds ../miscembeds/senna.txt --in_dim 50 --birnn --hid_dim 50"

echo 0

C=config_low0

for s in streusle semcor ccg ; do
    #t=streusle
    #t=semcor
    #t=ccg
    t=chunking
    echo $t
    
    echo "train "$t" data/eng_"$t"_train.conll 2" > $C
    echo "dev "$t" data/eng_"$t"_dev.conll 2" >> $C
    
    python2.7 mtl_tagger.py --cfg config_low0 --out out/out_low0 $OPTIONS
    python2.7 mtl_tagger.py TEST --model out/out_low0.$t.best_model --test $t:data/eng_$t"_test.conll"
    PRED=out/out_low0.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    
    echo 1
    
    C=config_low1
    echo "train "$s" data/eng_"$s"_train.conll 2" > $C
    echo "train "$t" data/eng_"$t"_train.conll 2" >> $C
    echo "dev "$s" data/eng_"$s"_dev.conll 2" >> $C
    echo "dev "$t" data/eng_"$t"_dev.conll 2" >> $C
    
    python2.7 mtl_tagger.py --cfg config_low1 --out out/out_low1 $OPTIONS
    python2.7 mtl_tagger.py TEST --model out/out_low1.$t.best_model --test $t:data/eng_$t"_test.conll"
    PRED=out/out_low1.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    
    echo 2
    
    C=config_low2
    echo "train "$s" data/eng_"$s"_train.conll 0" > $C
    echo "train "$t" data/eng_"$t"_train.conll 2" >> $C
    echo "dev "$s" data/eng_"$s"_dev.conll 0" >> $C
    echo "dev "$t" data/eng_"$t"_dev.conll 2" >> $C
    
    python2.7 mtl_tagger.py --cfg config_low2 --out out/out_low2 $OPTIONS
    python2.7 mtl_tagger.py TEST --model out/out_low2.$t.best_model --test $t:data/eng_$t"_test.conll"
    PRED=out/out_low2.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED

done
