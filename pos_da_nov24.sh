python2.7 mtl_tagger.py --cfg config_pos_da_nov24 --out out/out_pos_da_nov24 --iters 3 --embeds ../miscembeds/senna.txt --in_dim 50 --hid_dim 50
python2.7 mtl_tagger.py TEST --model out/out_pos_da_nov24.pos.best_model --test pos:data/gimpel.dev.nm.noslash
PRED=out/out_pos_da_nov24.pos.best_model.test.tagged.pos
python2.7 accuracy.py $PRED
python2.7 fscore.py $PRED
