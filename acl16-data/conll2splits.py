import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

sents=[]
sent=[]
for l in lines:
    if len(l)<2:
        sents.append(sent)
        sent=[]
    else:
        sent.append(l)

N=len(sents)

FoldSize=int(N/10)

M=1000

test=sents[:1000]
dev=sents[1000:2000]
train=sents[2000:]


out=open(sys.argv[1]+'.train','w')
for t in train:
    for w in t:
        print >>out, "\t".join(w)
    print >>out, ""
out.close()
out=open(sys.argv[1]+'.dev','w')
for t in dev:
    for w in t:
        print >>out, "\t".join(w)
    print >>out, ""
out.close()
out=open(sys.argv[1]+'.test','w')
for t in test:
    for w in t:
        print >>out, "\t".join(w)
    print >>out, ""
out.close()
