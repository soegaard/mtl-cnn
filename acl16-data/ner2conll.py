import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

for l in lines:
    if len(l)>1:
        print l[0]+'\t'+l[-1]
    else:
        print ""
        
