# Tweets for hashtag subtask 
Balanced.jtweet.conll.txt are japanese tweets randomly sampled from twitter
Hastags.jtweet.conll.txt are japanese tweets filtered by the existence of at least 1 hashtag (#) and tagged 1 if character is start of hashtag and else 0
tabs and newlines have been converted to spaces.


# files for pos tagging.

The KC files are the Kyoto corpus, split into train, test and dev, following the same split as CoNLL09 in CoNLL format. There are 90 tags (not all present in corpus) that represent both wordsegmentation, upper POS and lower POS.

Each character has been assigned 1 of these tags, depending on whether it is the first character in a word, and the part-of-speech of the word, that it is a part of.

jtweet.test.conll.txt is formatted as the KC files with POS tags that include B/I tags and can be used to *test* domain transfer. 