import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

sents=[]
sent=[]
for l in lines:
    if len(l)<2:
        sents.append(sent)
        sent=[]
    else:
        sent.append(l)

N=len(sents)

FoldSize=int(N/10)

M=1000

sample=sents[::100]

out=open(sys.argv[1]+'.sample','w')
for t in sample:
    for w in t:
        print >>out, "\t".join(w)
    print >>out, ""
out.close()
