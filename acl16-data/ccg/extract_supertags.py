from collections import Counter
import glob
import re
BASE = "../ccgbank_1_1/data/AUTO/%s/*"

lexpat = re.compile(r"<L([^>]*)>")

scnt = Counter()
for snum in range(0,25):
    sec = "%.2d" % snum
    outf = "data/%s.alltags" % sec
    fout = file(outf,"w")
    for fname in glob.glob(BASE % sec):
        for line in file(fname):
            if line.startswith("ID="): continue
            lex = [x.strip().split() for x in lexpat.findall(line.strip())]
            w_p_s = [(x[3],x[1],x[0]) for x in lex]
            if 2 <= snum <= 21:
                scnt.update([s for w,p,s in w_p_s])
            for item in w_p_s:
                print >> fout, "\t".join(item)
            print >> fout 
    fout.close()

cats = [ss for ss,c in scnt.most_common() if c>=10]
assert(len(cats) == 425)
file("data/categories","w").write("\n".join(cats))

cats = set(cats)
for snum in range(0,25):
    sec = "%.2d" % snum
    inf = "data/%s.alltags" % sec
    outf = "data/%s.common" % sec
    fout = file(outf,"w")
    for line in file(inf):
        line = line.strip().split()
        if not line:
            print >> fout
            continue
        w,p,s=line
        s = s if s in cats else "_"
        print >> fout, "\t".join([w,s])

