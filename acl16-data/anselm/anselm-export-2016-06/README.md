This folder contains all texts from the Anselm Corpus[^1] that have already been
manually normalized as of 20.06.2016.  This is almost all of them except for
some Low German or Dutch manuscripts; some also await another control &
correction pass over the annotations (and are therefore not yet "final").

Files are preprocessed in the following way:

* tokens containing only punctuation are removed (punctuation in historical
  texts is highly unreliable, and we don't want to evaluate normalization on
  them);
* instances of "ß" are replaced with "ss" (due to the annotators unreliably
  using old/new/Swiss spelling of words with "ß" -- should only affect the
  normalizations);
* all characters are lower-cased (capitalization is also highly unreliable).

Files shorter than 4,000 tokens have been moved to the subfolder `fragments/`.
44 files remain, with a total of 323,531 tokens.

Additionally, for normalizations of **extinct words** (marked as such by the
annotators), the **modernization layer** is used instead of the normalization.
The normalization in these cases is based on a Middle High German lexicon and
therefore not a valid modern German word.  On the flipside, the modernization
that's used instead is usually not a "spelling variant" of the original token,
but a completely different lexeme.  Alternative would be to remove these cases
entirely, but that feels like cheating.  From the 44 files mentioned above, this
affects a total of 7,584 tokens.

Other problematic cases in the data (with token counts):

* Partly unreadable words with "..." in them -- 43
* Punctuation not split up and removed correctly
    + colon (:) -- 71
    + question mark (?) -- 11
    + slashes (/) -- 4
    + maybe others?

But only affects a small number of tokens, so maybe just ignore for now?


[^1]: https://www.linguistics.rub.de/comphist/projects/anselm/
