DATE="nov30"
LOG="" #> log.run_yoav_new 2> log.run_yoav_new
OPTIONS="--iters 10 --embeds ../miscembeds/senna.txt --in_dim 50 --birnn --hid_dim 50"
echo bc
for final in names ; do #com ner sst frame ; do
    for aux in tags ; do
	C0=config0_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out0_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/eng_$final"_test.conll" data/ontonotes_bc_cctv.$final data/ontonotes_bc_cnn.$final data/ontonotes_bc_msnbc.$final data/ontonotes_bc_p2.5_a2e.$final data/ontonotes_bc_p2.5_c2e.$final data/ontonotes_bc_phoenix.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out0_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out0_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C0=config1_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "train "$aux" data/ontonotes_bc_cctv."$aux" 2" >> $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	echo "dev "$aux" data/ontonotes_bc_cnn."$aux" 2" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out1_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/eng_$final"_test.conll" data/ontonotes_bc_cctv.$final data/ontonotes_bc_cnn.$final data/ontonotes_bc_msnbc.$final data/ontonotes_bc_p2.5_a2e.$final data/ontonotes_bc_p2.5_c2e.$final data/ontonotes_bc_phoenix.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out1_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out1_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C0=config2_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "train "$aux" data/ontonotes_bc_cctv."$aux" 0" >> $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	echo "dev "$aux" data/ontonotes_bc_cnn."$aux" 0" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out2_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/eng_$final"_test.conll" data/ontonotes_bc_cctv.$final data/ontonotes_bc_cnn.$final data/ontonotes_bc_msnbc.$final data/ontonotes_bc_p2.5_a2e.$final data/ontonotes_bc_p2.5_c2e.$final data/ontonotes_bc_phoenix.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out2_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out2_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
    done
done

echo bn
for final in names ; do #com ner sst frame ; do
    for aux in tags ; do
	C0=config0_$final
	for TESTFILE in data/ontonotes_bn_abc.$final data/ontonotes_bn_cnn.$final data/ontonotes_bn_mnb.$final data/ontonotes_bc_nbc.$final data/ontonotes_bn_p2.5_a2e.$final data/ontonotes_bn_p2.5_c2e.$final data/ontonotes_bn_pri.$final data/ontonotes_bn_voa.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out0_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out0_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C0=config1_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "train "$aux" data/ontonotes_bn_abc."$aux" 2" >> $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	echo "dev "$aux" data/ontonotes_bn_cnn."$aux" 2" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out1_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/ontonotes_bn_abc.$final data/ontonotes_bn_cnn.$final data/ontonotes_bn_mnb.$final data/ontonotes_bc_nbc.$final data/ontonotes_bn_p2.5_a2e.$final data/ontonotes_bn_p2.5_c2e.$final data/ontonotes_bn_pri.$final data/ontonotes_bn_voa.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out1_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out1_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C0=config2_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "train "$aux" data/ontonotes_bn_abc."$aux" 0" >> $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	echo "dev "$aux" data/ontonotes_bn_cnn."$aux" 0" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out2_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/ontonotes_bn_abc.$final data/ontonotes_bn_cnn.$final data/ontonotes_bn_mnb.$final data/ontonotes_bc_nbc.$final data/ontonotes_bn_p2.5_a2e.$final data/ontonotes_bn_p2.5_c2e.$final data/ontonotes_bn_pri.$final data/ontonotes_bn_voa.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out2_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out2_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
    done
done

echo mz
for final in names ; do #com ner sst frame ; do
    for aux in tags ; do
	C0=config0_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out0_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/ontonotes_mz_sinorama.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out0_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out0_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C0=config1_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "train "$aux" data/ontonotes_mz_sinorama."$aux" 2" >> $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	echo "dev "$aux" data/ontonotes_mz_sinorama."$aux" 2" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out1_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/ontonotes_mz_sinorama.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out1_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out1_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C0=config2_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "train "$aux" data/ontonotes_mz_sinorama."$aux" 0" >> $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	echo "dev "$aux" data/ontonotes_mz_sinorama."$aux" 0" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out2_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/ontonotes_mz_sinorama.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out2_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out2_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
    done
done

echo wb
for final in names ; do #com ner sst frame ; do
    for aux in tags ; do
	C0=config0_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out0_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/ontonotes_wb_a2e.$final data/ontonotes_wb_c2e.$final data/ontonotes_wb_eng.$final data/ontonotes_wb_p2.5_a2e.$final data/ontonotes_wb_p2.5_c2e.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out0_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out0_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C0=config1_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "train "$aux" data/ontonotes_wb_a2e."$aux" 2" >> $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	echo "dev "$aux" data/ontonotes_wb_c2e."$aux" 2" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out1_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/ontonotes_wb_a2e.$final data/ontonotes_wb_c2e.$final data/ontonotes_wb_eng.$final data/ontonotes_wb_p2.5_a2e.$final data/ontonotes_wb_p2.5_c2e.$final data/ontonotes_wb_sel.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out1_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out1_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
	C0=config2_$final
	echo "train "$final" data/ontonotes_nw_wsj."$final" 2" > $C0
	echo "train "$aux" data/ontonotes_wb_a2e."$aux" 0" >> $C0
	echo "dev "$final" data/ontonotes_nw_p2.5_a2e."$final" 2" >> $C0
	echo "dev "$aux" data/ontonotes_wb_c2e."$aux" 0" >> $C0
	python2.7 mtl_tagger.py --cfg $C0 --out out/out2_$final"_"$DATE $OPTIONS $LOG
	for TESTFILE in data/ontonotes_wb_a2e.$final data/ontonotes_wb_c2e.$final data/ontonotes_wb_eng.$final data/ontonotes_wb_p2.5_a2e.$final data/ontonotes_wb_p2.5_c2e.$final data/ontonotes_wb_sel.$final ; do
	    echo $TESTFILE
	    python2.7 mtl_tagger.py TEST --model out/out2_$final"_"$DATE.$final.best_model --test $final:$TESTFILE $LOG
	    TEST=out/out2_$final"_"$DATE.$final.best_model.test.tagged.$final
	    python2.7 accuracy.py $TEST
	    python2.7 fscore.py $TEST
	done
    done
done

