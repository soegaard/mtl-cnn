from pycnn import *
from collections import defaultdict
from itertools import *
import random

from nnlib import *

from docopt import docopt

random.seed(1)

"""
for each task (input file in our case) specify:
    - which training files
        - from which layers should it be predicted.
    - which dev files
        - from which layer should it be predicted.
"""



args = docopt("""
    Usage: 
        mtl_tagger.py --cfg=<file> --out=<ofile> [options ]
        mtl_tagger.py TEST --model=<file> --test=<task:file> ...

    example of TEST command:
        mtl_tagger.py TEST --model out/themodel.chunks.best_model --test chunk:data/wsj/chunk/sec_20 --test pos:data/wsj/pos/sec_20

    Options:
        --iters k    [default: 10]
        --in_dim k   [default: 100]
        --hid_dim k  [default: 100]
        --rnn type   [default: lstm]  other options: simple
        --birnn      if on, use a birnn
        --activate k  [default: none] activation between RNN layers -- relu, tanh, none
        --embeds fname  [default: none]
        --keep_every k  [default: 5] keep a model every k iterations at the end.
        --skiplabel k   [default: _] skip training examples where the label is k
        --word_dropout k  [default: 0.1] apply word-dropout during trainign with rate k (drop if random < k)
        """)

def create_network_components(corpora, model):
    IN  = int(args['--in_dim'])
    HID = int(args['--hid_dim'])
    num_layers = corpora.max_layer
    # The INPUT (embeddings) layers
    vocab = list(sorted(corpora.vocab.keys()))

    # TODO: initialize from external embeddings?
    #lookup = model.add_lookup_parameters("lookup", (len(vocab), IN))
    embeds_file = args['--embeds'] if args['--embeds'] != 'none' else None
    embed = FFSequencePreidctor(FeaturesToDense(model, vocab, embeds_file, IN))

    # The RNN Layers
    layers = []
    rlayers = []
    for n in xrange(num_layers+1):
        builder = LSTMBuilder #SimpleRNNBuilder
        if args['--rnn'] != 'lstm':
            print "setting RNN-builder to SimpleRNN"
            builder = SimpleRNNBuilder
        if n == 0:
            layers.append(RNNSequencePredictor(builder(1, IN, HID, model))) 
            if (args['--birnn']):
                rlayers.append(RNNSequencePredictor(builder(1, IN, HID, model)))
        else:
            layers.append(RNNSequencePredictor(builder(1, HID, HID, model))) 
            if (args['--birnn']):
                rlayers.append(RNNSequencePredictor(builder(1, HID, HID, model)))
    # The prediction (softmax) layers
    preds = {}
    for task in corpora.tasks():
        nclasses = len(corpora.lvocab_for_task(task))
        for layer in corpora.task_id_to_layers[task]:
            if (args['--birnn']):
                preds[(task,layer)] = FFSequencePreidctor(Layer(model, HID*2, nclasses, softmax))
            else:
                preds[(task,layer)] = FFSequencePreidctor(Layer(model, HID, nclasses, softmax))
    return [embed, layers, rlayers, preds]
        
def build_and_predict(embed, layers, rlayers, preds, activate, task_id, input_sequence,
                      output_expected_at, output_sequence):
    # build the network
    # input layer
    input_vecs = embed.predict_sequence([[x] for x in input_sequence])
    # RNN layers + output predictions
    max_layer = max(output_expected_at)
    outputs = []
    prev_layer = input_vecs
    if args['--birnn']:
        rprev_layer = input_vecs
        for layer_id, layer, rlayer in zip(xrange(max_layer+1),layers, rlayers):
            layer_out = layer.predict_sequence(prev_layer)
            rlayer_out = list(reversed(rlayer.predict_sequence(reversed(rprev_layer))))
            if (activate and prev_layer != input_vecs):
                layer_out = map(activate,layer_out)
                rlayer_out = map(activate,rlayer_out)

            if layer_id in output_expected_at:
                #print [float(x) for x in layer_out]
                output_predictor = preds[(task_id, layer_id)]
                #concat_layer = [tanh(concatenate([f,r])) for f,r in zip(layer_out, rlayer_out)]
                concat_layer = [concatenate([f,r]) for f,r in zip(layer_out, rlayer_out)]
                outputs.append(output_predictor.predict_sequence(concat_layer))
            prev_layer = layer_out
            rprev_layer = rlayer_out
    else:
        for layer_id, layer in zip(xrange(max_layer+1),layers):
            layer_out = layer.predict_sequence(prev_layer)
            if layer_id in output_expected_at:
                output_predictor = preds[(task_id, layer_id)]
                outputs.append(output_predictor.predict_sequence(layer_out))
            prev_layer = layer_out
    return outputs

def eval_model(train_corpora, dev_corpora, embed, layers, rlayers, preds):
    good = defaultdict(float)
    bad  = defaultdict(float)
    birnn = args['--birnn']
    activate = {'relu':rectify,'tanh':tanh,'none':None}[args['--activate']]
    for sent_id, item in enumerate(dev_corpora.items,1):
        renew_cg()
        task_id, input_sequence, output_sequence = item
        l2i = train_corpora.label_mapper(task_id)
        i2l = {i:t for t,i in l2i.items()}
        i2l[-1] = args['--skiplabel']
        output_sequence = [l2i.get(o,-1) for o in output_sequence]
        output_expected_at = dev_corpora.task_id_to_layers[task_id]

        outputs = build_and_predict(embed,layers,rlayers,preds,activate,task_id,input_sequence,output_expected_at,output_sequence)
        predicted = outputs[0]
        assert(len(predicted) == len(output_sequence))
        for p,o in zip(predicted, output_sequence):
            p = p.value()
            p = np.argmax(p)
            if p == o:
                good[task_id] += 1
            else:
                bad[task_id] += 1
        if False:
            for i,p,o in zip(input_sequence,predicted, output_sequence):
                p = p.value()
                p = np.argmax(p)
                print i,"\t",i2l[o],"\t",i2l[p]
            print
    for task in dev_corpora.tasks():
        print "\t",task,"\t", good[task] / (good[task]+bad[task])
    return {task:(good[task]/(good[task]+bad[task])) for task in dev_corpora.tasks()}

def tag_sents(train_corpora, to_tag_corpora, embed, layers, rlayers, preds, outfile):
    birnn = args['--birnn']
    activate = {'relu':rectify,'tanh':tanh,'none':None}[args['--activate']]
    outf = {}
    for t in to_tag_corpora.tasks():
        outf[t] = file(outfile+"."+t,"w")

    for sent_id, item in enumerate(to_tag_corpora.items,1):
        renew_cg()
        task_id, input_sequence, output_sequence = item
        l2i = train_corpora.label_mapper(task_id)
        i2l = {i:t for t,i in l2i.items()}
        i2l[-1] = args['--skiplabel']
        output_sequence = [l2i.get(o,-1) for o in output_sequence]
        output_expected_at = to_tag_corpora.task_id_to_layers[task_id]

        outputs = build_and_predict(embed,layers,rlayers,preds,activate,task_id,input_sequence,output_expected_at,output_sequence)
        predicted = outputs[0]
        assert(len(predicted) == len(output_sequence))
        for i,p,o in zip(input_sequence,predicted, output_sequence):
            p = p.value()
            p = np.argmax(p)
            print >> outf[task_id],i,"\t",i2l[o],"\t",i2l[p]
        print >> outf[task_id]
    for t in to_tag_corpora.tasks():
        outf[t].close()

def load_and_classify(args,modelfile,train_corpora,test_corpora):
    model = Model()
    embed, layers, rlayers, preds = create_network_components(train_corpora, model)
    activate = {'relu':rectify,'tanh':tanh,'none':None}[args['--activate']]
    model.load(modelfile)
    tag_sents(train_corpora, test_corpora, embed, layers, rlayers, preds, modelfile+".test.tagged")

def inspect_model_for_saving(scores, best_scores, I, sent_id, fbase, model):
    for task, score in scores.items():
        if score > best_scores[task]:
            model.save(fbase + "." + task + ".best_model")
            with open(fbase + ".log", "a") as fout:
                print >> fout, "\t".join([task,  "%s-%s" % (I, sent_id), str(scores[task])])
            best_scores[task] = score
    return best_scores

def train(corpora, dev_corpora):
    word_dropout = float(args['--word_dropout'])
    with open(args['--out']+".cfg","w") as f: f.write(str(args))
    model = Model()
    embed, layers, rlayers, preds = create_network_components(corpora, model)
    if args['--birnn']: assert(len(rlayers)==len(layers))
    else: assert(len(rlayers)==0)
    trainer = SimpleSGDTrainer(model)
    activate = {'relu':rectify,'tanh':tanh,'none':None}[args['--activate']]
    best_scores = defaultdict(float)
    for I in xrange(int(args['--iters'])):
        total_loss = 0.0
        random.shuffle(corpora.items)
        print "num sentences:",len(corpora.items)
        for sent_id, item in enumerate(corpora.items,1):
            renew_cg()
            task_id, input_sequence, output_sequence = item
            l2i = corpora.label_mapper(task_id)
            output_sequence = [l2i.get(o, -1) for o in output_sequence] # -1 for the skipped label
            output_expected_at = corpora.task_id_to_layers[task_id]

            input_sequence = [x if random.random() > word_dropout else "__UNK__" for x in input_sequence ]
            outputs = build_and_predict(embed,layers,rlayers,preds,activate,task_id,input_sequence,output_expected_at,output_sequence)
            # each element in outputs is a sequence of softmaxes.
            # calculate the losses
            losses = []
            for output in outputs:
                assert(len(output) == len(output_sequence))
                for out, expected in zip(output, output_sequence):
                    if expected >= 0: losses.append(-log(pick(out, expected)))
            loss = esum(losses)
            total_loss += loss.value()
            loss.backward()
            trainer.update()
            if sent_id % 100 == 0:
                print "loss:", total_loss / sent_id

            if sent_id % 5000 == 0:
                print "after",I,sent_id,"examples"
                scores = eval_model(corpora, dev_corpora, embed, layers, rlayers, preds)
                best_scores = inspect_model_for_saving(scores, best_scores, I, sent_id, args['--out'], model)
                print
        print "total_loss:",total_loss
        tag_sents(corpora, dev_corpora, embed, layers, rlayers, preds, args['--out']+"."+str(I)+".tagged")
        scores = eval_model(corpora, dev_corpora, embed, layers, rlayers, preds)
        best_scores = inspect_model_for_saving(scores, best_scores, I, sent_id, args['--out'], model)
        #if (I % int(args['--keep_every']) == 0):
            #model.save(args['--out'] + "." + str(I))
    # after the loop
    #model.save(args['--out'] + "." + str(I))
                    
from dio import *
if args['TEST']:
    modelfile = args['--model']
    args0 = args
    base, task, best_model = modelfile.rsplit(".",2)
    args = eval(file(base+".cfg").read())
    config = read_config(args['--cfg'])
    test_corpora = config['dev']
    test_corpora.items = []
    for task_data in args0['--test']:
        taskname, taskfile = task_data.split(":")
        test_corpora.add_corpus(taskname, read_two_cols_data(taskfile))
    load_and_classify(args,modelfile,config['train'],test_corpora)
else:
    config = read_config(args['--cfg'])
    config['train'].set_ignore_label(args['--skiplabel'])
    train(config['train'], config['dev'])

