from pycnn import *
from collections import defaultdict
from itertools import *

global_counter = count(0)

def load_embeddings(fname):
    import numpy as np
    f = (line.split(" ",1)[1] for line in file(fname))
    vecs = np.loadtxt(f,skiprows=1)
    words = [line.split(" ",1)[0].split("_")[0] for line in file(fname)][1:]
    print words[:10]
    return words,vecs

class SequencePredictor:
    def __init__(self):
        pass

    def predict_sequence(self, inputs):
        raise "Not Implmented"

class FFSequencePreidctor(SequencePredictor):
    def __init__(self, network_builder):
        self.network_builder = network_builder

    def predict_sequence(self, inputs):
        network_builder = self.network_builder
        outputs = [network_builder(x) for x in inputs]
        return outputs

class RNNSequencePredictor(SequencePredictor):
    def __init__(self, rnn_builder):
        """
        rnn_builder: a LSTMBuilder or SimpleRNNBuilder object.
        """
        self.builder = rnn_builder

    def predict_sequence(self, inputs):
        s = self.builder.initial_state()
        outputs = []
        for x in inputs:
            s = s.add_input(x)
            outputs.append(s.output())
        return outputs

class Layer:
    def __init__(self, model, in_dim, output_dim, activation=rectify):
        ident = str(global_counter.next())
        self.act = activation
        self.W = model.add_parameters("W_"+ident, (output_dim, in_dim))
        self.b = model.add_parameters("b_"+ident, (output_dim))

    def __call__(self, x):
        W = parameter(self.W)
        b = parameter(self.b)
        return self.act(W*x + b)

class FeaturesToDense:
    """
    TODO: deal with unknown features
    """
    def __init__(self, model, possible_features, embeds_file, emb_dim):
        ident = str(global_counter.next())
        emb = {}
        if embeds_file:
            print "reading embeds file"
            words,vecs = load_embeddings(embeds_file)
            assert( vecs.shape[1] == emb_dim )
            print "done",len(words),len(vecs)
            print len(set(words).intersection(set(possible_features)))
            possible_features = list(set(words).union(set(possible_features)))
            emb = {w:v for w,v in izip(words,vecs)}
        else:
            print "warning: no embeds file"
        self.w2i = {w:i for i,w in enumerate(["__UNK__"] + possible_features)}
        self.fembeds = model.add_lookup_parameters(
                "lookup"+ident, 
                (len(self.w2i.keys()), emb_dim))
        init = 0
        unk = 0
        for w,i in self.w2i.iteritems():
            if w in emb:
                self.fembeds.init_row(i, emb[w])
                init += 1
            else:
                unk += 1
        print "initialized",init,"words",unk,"unknowns"

    def __call__(self, features):
        lookup = self.fembeds
        features = [self.w2i.get(f,0) for f in features]
        return concatenate([lookup[f] for f in features])




