DATE="dec15"
TGR="mtl_tagger.py"
LOG="" #> log.run_yoav_new 2> log.run_yoav_new
OPTIONS="--iters 10 --embeds ../miscembeds/senna.txt --in_dim 50 --birnn --hid_dim 50"

C0=config0_alltask

echo "train pos data/eng_pos_train.conll 0" > $C0
for task in ccg chunking com fn semcor streusle ; do
    echo "train "$task" data/eng_"$task"_train.conll 2" >> $C0
    echo "dev "$task" data/eng_"$task"_train.conll 2" >> $C0
done
python2.7 $TGR --cfg $C0 --out out/out_all_$DATE $OPTIONS $LOG
for task in ccg chunking com fn semcor ; do
    python2.7 $TGR TEST --model out/out0_all"_"$DATE.$task.best_model --test $task":eng_"$task"_test.conll" $LOG
    TEST=out/out0_all"_"$DATE.$task.best_model.test.tagged.$task
    python2.7 accuracy.py $TEST
    python2.7 fscore.py $TEST
done


