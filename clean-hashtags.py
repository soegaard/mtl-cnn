import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

i=0
Break=False
for l in lines:
    if i<5000 and len(l)==2:
        if l[0]=="<SP>":
            Break=True
            i+=1
        else:
            if Break:
                print ""
            else:
                print '\t'.join(l)
            Break=False
