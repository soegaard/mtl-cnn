for embs in klementiev ; do #chandar omer-europarl ; do
    embsname="/home/anders/bible-embs/naacl16/embs/subset/"+$lng+"."+$embs+".vecs"
    OPTIONS="--iters 10 --embeds "$embsname" --in_dim 30 --birnn --hid_dim 30"
    echo $lng $embs
    EXP=$lng.$embs
    
    C=config_$EXP
    echo "train fn data/eng_fn_train.conll 1" > $C
    echo "dev fn data/eng_fn_test.conll 1" >> $C

    python2.7 mtl_tagger.py --cfg $PWD/config_$EXP --out $PWD/out/out_$EXP $OPT
    TESTFILE=$PWD/data/$lng"_fn_test.conll"
    python2.7 mtl_tagger.py TEST --model $PWD/out/out_$EXP.com.best_model --test fn:$TESTFILE
    echo $TESTFILE
    python2.7 accuracy.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
    python2.7 fscore.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
done
