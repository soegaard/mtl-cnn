import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

FirstPassed=False
for l in lines:
    if len(l)>1:
        if FirstPassed and l[0][0].istitle():
            print l[0]+'\tTITLE'
        else:
            print l[0]+'\tO'
        if not FirstPassed:
            FirstPassed=True
    else:
        print ""
        FirstPassed=False
