FIX=ner_da_nov24
python2.7 mtl_tagger.py --cfg config_$FIX --out out/out_$FIX --iters 10 --embeds ../miscembeds/ew30.txt --in_dim 30
python2.7 mtl_tagger.py TEST --model out/out_$FIX.ner.best_model --test ner:data/mz.crf.conll.1k
PRED=out/out_$FIX.ner.best_model.test.tagged.ner
python2.7 accuracy.py $PRED
python2.7 fscore.py $PRED
