OPTIONS="--iters 10 --embeds ../miscembeds/senna.txt --in_dim 50 --birnn --hid_dim 50"

echo 0

C=config_misc

for t in streusle semcor ccg ; do
    #t=streusle
    #t=semcor
    #t=ccg
    echo $t
    
    echo "train "$t" data/eng_"$t"_train.conll 2" > $C
    echo "dev "$t" data/eng_"$t"_dev.conll 2" >> $C
    
    python2.7 mtl_tagger.py --cfg config_misc --out out/out_misc $OPTIONS
    python2.7 mtl_tagger.py TEST --model out/out_misc.$t.best_model --test $t:data/eng_$t"_test.conll"
    PRED=out/out_misc.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    
    echo 1
    
    C=config_misc
    echo "train pos data/ontonotes_nw_wsj.tags 2" > $C
    echo "train "$t" data/eng_"$t"_train.conll 2" >> $C
    echo "dev pos data/ontonotes_nw_p2.5_a2e.tags 2" >> $C
    echo "dev "$t" data/eng_"$t"_dev.conll 2" >> $C
    
    python2.7 mtl_tagger.py --cfg config_misc --out out/out_misc $OPTIONS
    python2.7 mtl_tagger.py TEST --model out/out_misc.$t.best_model --test $t:data/eng_$t"_test.conll"
    PRED=out/out_misc.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    
    echo 2
    
    C=config_misc
    echo "train pos data/ontonotes_nw_wsj.tags 0" > $C
    echo "train "$t" data/eng_"$t"_train.conll 2" >> $C
    echo "dev pos data/ontonotes_nw_p2.5_a2e.tags 0" >> $C
    echo "dev "$t" data/eng_"$t"_dev.conll 2" >> $C
    
    python2.7 mtl_tagger.py --cfg config_misc --out out/out_misc $OPTIONS
    python2.7 mtl_tagger.py TEST --model out/out_misc.$t.best_model --test $t:data/eng_$t"_test.conll"
    PRED=out/out_misc.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED

done
