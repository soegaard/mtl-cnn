OPTIONS="--iters 30 --in_dim 64 --birnn --hid_dim 64"
CNNSEED=""
echo 0

# chbt.B1.test

C=config_low0

for t in B1 ; do
    #t=streusle
    #t=semcor
    #t=B1
    #t=chunking
    echo $t

    for s in B1 Me1 N2 St1 ; do
	echo "train "$s" data/chbt."$s".train 2" > $C
	echo "dev "$s" data/chbt."$s".dev 2" >> $C
    
#	python2.7 mtl_tagger.py $CNNSEED --cfg config_low0 --out out/out_low0 $OPTIONS
	python2.7 mtl_tagger.py TEST --model out/out_low0.$s.best_model --test $s:data/chbt.$s".test"
	PRED=out/out_low0.$s.best_model.test.tagged.$s
	echo $PRED
	python2.7 accuracy.py $PRED
	python2.7 fscore.py $PRED
	python2.7 complete.py $PRED
    done
    echo 1
    
    C=config_low1
    echo "train "$t" data/chbt."$t".train 2" > $C
    echo "dev "$t" data/chbt."$t".dev 2" >> $C
    echo "train celex data/celex_german_aligned_with_markers.txt.sample 0" >> $C
    echo "dev celex data/celex_german_aligned_with_markers.txt.sample 0" >> $C
    for s in Me1 N2 St1 ; do 
	echo "train "$s" data/chbt."$s".train 2" >> $C
	echo "dev "$s" data/chbt."$s".dev 2" >> $C
    done
    
    python2.7 mtl_tagger.py $CNNSEED --cfg config_low1 --out out/out_low1 $OPTIONS
    for s in B1 Me1 N2 St1 ; do 
	python2.7 mtl_tagger.py TEST --model out/out_low1.$s.best_model --test $s:data/chbt.$s".test"
	PRED=out/out_low1.$s.best_model.test.tagged.$s
	echo $PRED
	python2.7 accuracy.py $PRED
	python2.7 fscore.py $PRED
	python2.7 complete.py $PRED
    done
#    echo 2
    
#    C=config_low2
#    echo "train "$t" data/chbt."$t".train 2" > $C
#    echo "dev "$t" data/chbt."$t".dev 2" >> $C
#    for s in Me1 N2 St1 ; do 
#	echo "train "$s" data/chbt."$t".train 0" >> $C
#	echo "dev "$s" data/chbt."$t".dev 0" >> $C
#    done
    
#    python2.7 mtl_tagger.py $CNNSEED --cfg config_low2 --out out/out_low2 $OPTIONS
#    for s in B1 Me1 N2 St1 ; do 
#	python2.7 mtl_tagger.py TEST --model out/out_low2.$s.best_model --test $s:data/chbt.$s".test"
#	PRED=out/out_low2.$s.best_model.test.tagged.$s
#	echo $PRED
#	python2.7 accuracy.py $PRED
#	python2.7 fscore.py $PRED
#	python2.7 complete.py $PRED
#    done
    
done
