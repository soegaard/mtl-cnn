good = bad = 0.0
import sys
for line in sys.stdin:
    line = line.strip().split()
    if not line: continue
    if line[-1]==line[-2]:
        good +=1
    else: bad +=1
print good / (good+bad)
