import sys
"""
Gen config files for Chunk+POS transfer learning, within domain.

test always section 20
dev always sections 21-22

train is taken from 00 to 19, usually 15-18

20 is test for CoNLL 2000, 15-18 are train for CoNLL 2000, and used is C&W

The conditions are:
    chunk+pos, diff data, various sizes for POS:
        chunk: 15-18 * 4
        pos: 10-14, 8-14, 6-14, 4-14, 2-14

        

for each condition:
    both in 0
    both in 1
    both in 2
    pos in 0, chunk in 1
    pos in 0, chunk in 2
"""

CHUNK_FILE= "data/wsj/chunk/sec_%s" 
POS_FILE=   "data/wsj/pos/sec_%s"

DEV=["21","22"]

def gen(which, train_secs, dev_secs, layers, outf=sys.stdout):
    if not layers: return
    if which[0] == 'c':
        which = 'chunk'
        fname = CHUNK_FILE
    elif which[0] == 'p':
        which = 'pos'
        fname = POS_FILE
    else:
        assert False

    for sec in train_secs:
        print >> outf, "train", which, fname % sec, ",".join(map(str,layers))
    for sec in dev_secs:
        print >> outf, "dev", which, fname % sec, max(layers)
    
CDEF=["15","16","17","18"]
for pl,cl in [([0],[0]),([1],[1]),([2],[2]),([0],[1]),([0],[2])]:
    plf=",".join(map(str,pl))
    clf=",".join(map(str,cl))
    if not pl: plf="None"
    if not cl: clf="None"

    #chunk+pos, diff data, various sizes for POS:
    #    chunk: 15-18
    #    pos: 10-14, 8-14, 6-14, 4-14, 2-14
    E = "14"
    for S in [10,8,6,4,2]:
        PDEF = ["%02d" % i for i in xrange(int(S),int(E)+1)]
        with file("cfg2/pos%s-%s_%s_chunk15-18x4_%s.cfg" % (S,E,plf,clf), "w") as outf:
            gen('p',PDEF, DEV, pl, outf)
            gen('c',CDEF, DEV, cl, outf)
            gen('c',CDEF, DEV, cl, outf)
            gen('c',CDEF, DEV, cl, outf)
            gen('c',CDEF, DEV, cl, outf)

