from __future__ import division
import sys

lines=[l.strip().split() for l in open(sys.argv[1]).readlines()]

hit,tot,sent,sent_tot,last=0,0,0,0,0
for l in lines:
    if len(l)>1:
        if l[1]==l[2]:
            hit+=1
            last=1
        else:
            last=0
        tot+=1
    else:
        if last==1:
            sent+=1
        sent_tot+=1
print "accuracy:", hit/tot, sent/sent_tot
