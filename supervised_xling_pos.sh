for L in ko ; do # de
    for M in bl joint piped ; do
	echo "****" $L $M "****"
	C=config_count_embs
	echo "train "$L" data/ko-universal-train.conll.tt 2" > $C
	echo "dev "$L" data/ko-universal-dev.conll.tt 2" >> $C
	if [ $M=="joint" ]
	then
	    echo "train ja data/ja-universal-dev.conll.tt 2" >> $C
	    echo "dev ja data/ja-universal-test.conll.tt 2" >> $C
	    echo "train id data/id-universal-dev.conll.tt 2" >> $C
	    echo "dev id data/id-universal-test.conll.tt 2" >> $C 
	elif [ $M=="piped" ]
	then
	    echo "train ja data/ja-universal-dev.conll.tt 0" >> $C
	    echo "dev ja data/ja-universal-test.conll.tt 0" >> $C
	    echo "train id data/id-universal-dev.conll.tt 0" >> $C
	    echo "dev id data/id-universal-test.conll.tt 0" >> $C
	fi
	
	python2.7 mtl_tagger.py --cfg config_count_embs --out out/out_count_embs --iters 30 --in_dim 64 --birnn --hid_dim 64
	python2.7 mtl_tagger.py TEST --model out/out_count_embs.$L.best_model --test $L:data/$L-universal-test.conll.tt
	PRED=out/out_count_embs.$L.best_model.test.tagged.$L
	python2.7 accuracy.py $PRED
	python2.7 fscore.py $PRED
    done
done
