from __future__ import division
import sys,re
from collections import Counter as C

lines=[l.strip().split() for l in open(sys.argv[1]).readlines() if not re.compile("^\s*$").match(l)]

#micro f1

pred_dict,gold_dict,pred_last_word,gold_last_word=[],[],[],[]
prec,rec,pos,tot=0,0,0,0
prev_pred_tag="NULL"
prev_gold_tag="NULL"
for l in lines:
    if len(l)>1:
        if l[2][0]=="b":
            pred_dict.append("_".join(pred_last_word+[prev_pred_tag]))
            pred_last_word.append(l[0])
        else:
            pred_last_word.append(l[0])
            prev_pred_tag=l[2][1:]
        if l[1][0]=="b":
            gold_dict.append("_".join(gold_last_word+[prev_gold_tag]))
            gold_last_word.append(l[0])
        else:
            gold_last_word.append(l[0])
            prev_gold_tag=l[1][1:]

pred_dict=C(pred_dict)
gold_dict=C(gold_dict)
intersect=pred_dict&gold_dict

G=sum(gold_dict.values())
P=sum(pred_dict.values())
I=sum(intersect.values())

rec=I/G
prec=I/P

print "fscore:", (2*prec*rec)/(prec+rec), prec, rec
