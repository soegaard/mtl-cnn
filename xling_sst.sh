OPTIONS="--iters 1 --embeds ew30.txt --in_dim 30 --birnn --hid_dim 30"
echo baseline, senna, zd
EXP="baseline"

C=config_$EXP
echo "train sst data/eng_semcor_train.conll 2" > $C
echo "dev sst data/politiken_2009-5.tsv.conll 2" >> $C

python2.7 mtl_tagger.py --cfg $PWD/config_$EXP --out $PWD/out/out_$EXP $OPT
for TESTFILE in $PWD/data/eng_sst_test.conll ; do
    python2.7 mtl_tagger.py TEST --model $PWD/out/out_$EXP.com.best_model --test sst:$TESTFILE
    echo $TESTFILE
    python2.7 accuracy.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
    python2.7 fscore.py $PWD/out/out_$EXP.com.best_model.test.tagged.com
done
