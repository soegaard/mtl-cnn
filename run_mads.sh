OPTIONS="--iters 10 --in_dim 32 --birnn --hid_dim 32"
CNNSEED=""
echo 0

C=config_low0

for s in knbc kweb ; do
    #t=streusle
    #t=semcor
    t=mads-jap
    #t=chunking
    echo $t
    
    echo "train "$t" data/jap_"$t"_train.conll 2" > $C
    echo "dev "$t" data/jap_"$t"_dev.conll 2" >> $C
    
#    python2.7 mtl_tagger.py $CNNSEED --cfg config_low0 --out out/out_low0 $OPTIONS
    python2.7 mtl_tagger.py TEST --model out/out_low0.$t.best_model --test $t:data/jap_$t"_test.conll"
    PRED=out/out_low0.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    python2.7 mtl_tagger.py TEST --model out/out_low0.$t.best_model --test $t:data/twitter-jap_$t"_test.conll"
    PRED=out/out_low0.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    python2.7 wl-fscore.py $PRED
    
    echo 1
    
    C=config_low1
    echo "train "$s" data/jap_"$s"_train.conll 2" > $C
    echo "train "$t" data/jap_"$t"_train.conll 2" >> $C
    echo "dev "$s" data/jap_"$s"_train.conll 2" >> $C
    echo "dev "$t" data/jap_"$t"_dev.conll 2" >> $C

    python2.7 mtl_tagger.py $CNNSEED --cfg config_low1 --out out/out_low1 $OPTIONS
    python2.7 mtl_tagger.py TEST --model out/out_low1.$t.best_model --test $t:data/jap_$t"_test.conll"
    PRED=out/out_low1.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    python2.7 mtl_tagger.py TEST --model out/out_low1.$t.best_model --test $t:data/twitter-jap_$t"_test.conll"
    PRED=out/out_low1.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    python2.7 wl-fscore.py $PRED    

    echo 2
    
    C=config_low2
    echo "train "$s" data/jap_"$s"_train.conll 0" > $C
    echo "train "$t" data/jap_"$t"_train.conll 2" >> $C
    echo "dev "$s" data/jap_"$s"_train.conll 0" >> $C
    echo "dev "$t" data/jap_"$t"_dev.conll 2" >> $C
    
    python2.7 mtl_tagger.py $CNNSEED --cfg config_low2 --out out/out_low2 $OPTIONS
    python2.7 mtl_tagger.py TEST --model out/out_low2.$t.best_model --test $t:data/jap_$t"_test.conll"
    PRED=out/out_low2.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    python2.7 mtl_tagger.py TEST --model out/out_low2.$t.best_model --test $t:data/twitter-jap_$t"_test.conll"
    PRED=out/out_low2.$t.best_model.test.tagged.$t
    python2.7 accuracy.py $PRED
    python2.7 fscore.py $PRED
    python2.7 wl-fscore.py $PRED    
done
